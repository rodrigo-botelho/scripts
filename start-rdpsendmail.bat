set PROJECT_HOME=%DP_SERVER_SIDE_WORKSPACE_HOME%\rdp-sendmail
set BUILD_DIR=%PROJECT_HOME%\target
title %PROJECT_HOME%
cd %PROJECT_HOME%
mvn clean package -Dmaven.test.skip=true && cmd /V /C "dir /b %BUILD_DIR%\*.jar > %BUILD_DIR%\tmp.txt && set /p JAR_NAME=<%BUILD_DIR%\tmp.txt && java -jar -Xmx256m %BUILD_DIR%\!JAR_NAME!"

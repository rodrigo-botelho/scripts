#!/bin/bash

print_project_info() {
	cd $workspace
	cd $1
	branch=`git branch | grep \* | cut -d ' ' -f2`
	version=`cat build.gradle | grep 'version = ' | cut -d "'" -f2`
	if [ -z "$version" -a "$version" != " " ]; then
		version=`cat build.gradle | grep "version '" | cut -d "'" -f2`
	fi
	printf "%30s %30s %30s\n" $1 $branch $version
}

workspace='/c/Dev/Workspaces/dp/server-side/'

if [ ! -z "$1" -a "$1" != " " ]; then
	workspace=$1
fi

print_project_info './eurekaserver/'
print_project_info './rdpauthentication/'
print_project_info './rdpgateway/'
print_project_info './rdp-activities/'
print_project_info './rdp-annotations/'
print_project_info './rdp-forum/'
print_project_info './rdpmanager/RDP-Lex/'
print_project_info './rdp-notifications/'
print_project_info './rdp-offers/'
print_project_info './rdp-storage/'
print_project_info './rdp-user-groups-sync/'
print_project_info './rdpmanager/RDP-Rest-API/'

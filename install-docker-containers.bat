docker run --name some-mongo -p 27017:27017 -d mongo
docker run -d --hostname localhost --name some-rabbit -p 4369:4369 -p 5671:5671 -p 5672:5672 -p 25672:25671 -p 15671:15671 -p 15672:15672 rabbitmq:3-management
docker run --name some-postgres -p 5432:5432 -e POSTGRES_PASSWORD=root -d postgres
pause

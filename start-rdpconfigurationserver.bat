set PROJECT_HOME=%DP_SERVER_SIDE_WORKSPACE_HOME%\rdp-configuration-server
set BUILD_DIR=%PROJECT_HOME%\build\libs
title %PROJECT_HOME%
cd %PROJECT_HOME%
gradlew clean build -xtest && cmd /V /C "dir /b %BUILD_DIR%\*.jar > %BUILD_DIR%\tmp.txt && set /p JAR_NAME=<%BUILD_DIR%\tmp.txt && java -jar -Xmx256m %BUILD_DIR%\!JAR_NAME!"

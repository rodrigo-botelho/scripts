package br.com.digitalpages.mongodumprestore;

import java.time.Instant;

public class Main {
    static class MongoInfo {
        String user;
        String password;
        String host;
        String replicaSet;
        String authenticationDatabase;
        String database;
        String archive;

        boolean zip;
        boolean ssl;
    }

    static class MongoURIException extends Exception {
        MongoURIException(String message) {
            super(message);
        }
    }

    public static void main(String[] args) {
        if (args.length != 1 && args.length != 2) {
            printUsageAndExit();
        }

        try {
            String mongoURI = args[0];
            MongoInfo info = getMongoInfo(mongoURI);
            String date = Instant.now().toString();
            info.database = args.length == 2 ? args[1] : null;
            info.zip = true;
            info.archive = "backup-" + date.substring(0, date.indexOf("T")) + (info.database != null ? "-" + info.database : "") + ".gz";

            MongoInfo infoLocal = getMongoInfo("mongodb://localhost:27017/");
            infoLocal.database = info.database;
            infoLocal.zip = info.zip;
            infoLocal.archive = info.archive;

            System.out.println();
            System.out.println("docker exec -it some-mongo bash");
            System.out.println();
            printMongoCommand("mongodump", info);
            System.out.println();
            printMongoCommand("mongorestore", info);
            System.out.println();
            printMongoCommand("mongorestore", infoLocal);
            System.out.println();
        } catch (MongoURIException e) {
            printExceptionAndExit(e);
        }
    }

    private static void printUsageAndExit() {
        System.out.println("Usage: este-executavel mongo-uri [database]");
        System.exit(0);
    }

    private static void printExceptionAndExit(MongoURIException exception) {
        System.out.println("Erro: " + exception.getMessage());
        System.exit(0);
    }

    private static void printMongoCommand(String command, MongoInfo info) {
        System.out.print(command);

        if (info.host != null) {
            System.out.print(" --host=" + (info.replicaSet != null ? info.replicaSet + "/" : "") + info.host);
        }

        if (info.ssl) {
            System.out.print(" --ssl");
        }

        if (info.user != null) {
            System.out.print(" --username=" + info.user);
        }

        if (info.password != null) {
            System.out.print(" --password=" + info.password);
        }

        if (info.authenticationDatabase != null) {
            System.out.print(" --authenticationDatabase=" + info.authenticationDatabase);
        }

        if (info.database != null) {
            System.out.print(" --db=" + info.database);
        }

        if (info.archive != null) {
            System.out.print(" --archive=" + info.archive);
        }

        if (info.zip) {
            System.out.print(" --gzip");
        }

        System.out.println();
    }

    private static MongoInfo getMongoInfo(String mongoURI) throws MongoURIException {
        MongoInfo info = new MongoInfo();

        final String mongoStart = "mongodb://";
        final String mongoURILower = mongoURI.toLowerCase();
        int startIndex, endIndex, auxIndex;

        if (!mongoURILower.startsWith(mongoStart)) {
            throw new MongoURIException("A string de conexao deve comecar com " + mongoStart);
        }

        startIndex = mongoStart.length();
        endIndex = mongoURI.indexOf(":", startIndex);
        auxIndex = mongoURI.indexOf("@", startIndex);

        if (endIndex >= 0 && endIndex < auxIndex) {
            info.user = mongoURI.substring(startIndex, endIndex);

            startIndex = endIndex + 1;
            endIndex = mongoURI.indexOf("@", startIndex);
            if (endIndex == -1) {
                throw new MongoURIException("Senha nao encontrada na string");
            }
            info.password = mongoURI.substring(startIndex, endIndex);

            startIndex = endIndex + 1;
            endIndex = mongoURI.indexOf("/", startIndex);
            if (endIndex == -1) {
                endIndex = mongoURI.length();
            }
            info.host = mongoURI.substring(startIndex, endIndex);
        } else {
            endIndex = mongoURI.indexOf("/", startIndex);
            if (endIndex == -1) {
                endIndex = mongoURI.length();
            }
            info.host = mongoURI.substring(startIndex, endIndex);
        }

        startIndex = mongoURILower.indexOf("authsource=");
        if (startIndex >= 0) {
            startIndex += "authsource=".length();
            endIndex = mongoURILower.indexOf("&", startIndex);
            if (endIndex == -1) {
                endIndex = mongoURI.length();
            }
            info.authenticationDatabase = mongoURI.substring(startIndex, endIndex);
        }

        startIndex = mongoURILower.indexOf("replicaset=");
        if (startIndex >= 0) {
            startIndex += "replicaset=".length();
            endIndex = mongoURILower.indexOf("&", startIndex);
            if (endIndex == -1) {
                endIndex = mongoURI.length();
            }
            info.replicaSet = mongoURI.substring(startIndex, endIndex);
        }

        info.ssl = mongoURILower.contains("ssl=true");

        return info;
    }
}
